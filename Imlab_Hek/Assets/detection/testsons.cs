﻿using UnityEngine;
using LowLatencyMultichannelAudio;

public class testsons : MonoBehaviour
{
    private int StreamID;
    public float test;
    void Start()
    {
        Debug.Log("- Press Alpha1 - Alpha7 to play one shot.");
        Debug.Log("- Press Alpha8 to play looping, and Alpha9 to stop.");
        Debug.Log("- Press Space to stop all.");
    }
    float frac(float s) { return s - Mathf.Floor(s); }
    void Update()
    {
        float t = Mathf.Floor(frac(Time.time*0.1f)*8.0f);
        /* A. play one shot */
       
            // AudioManager.Asio.Play(3, new[] { 1 });*/
            if (t == 0)
            { AudioManager.Asio.Play(3, new[] { 0 }, volume: test); }
            if (t == 1)
            {
                AudioManager.Asio.Play(3, new[] { 1 }, volume: test);
            }
            if (t == 2)
            {
                AudioManager.Asio.Play(3, new[] { 2 }, volume: test);
            }
            if (t == 3)
            {
                AudioManager.Asio.Play(3, new[] { 3 }, volume: test);
            }
            if (t == 4)
            {
                AudioManager.Asio.Play(3, new[] { 4 }, volume: test);
            }
            if (t == 5)
            {
                AudioManager.Asio.Play(3, new[] { 5 }, volume: test);
            }
            if (t == 6)
            {
                AudioManager.Asio.Play(3, new[] { 6 }, volume: test);
            }
            if (t == 7)
            { AudioManager.Asio.Play(3, new[] { 7 }, volume: test); }

        
        /* B. play looping */
        if (Input.GetKeyDown(KeyCode.Alpha8) && StreamID <= 0)
            StreamID = AudioManager.Asio.Play(3, new[] { 0, 1 }, fadeSec: 1.0f, looping: true);

        /* C. stop B. */
        if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            AudioManager.Asio.Stop(StreamID, fadeSec: 1.0f);
            StreamID = 0;
        }

        /* D. stop all */
        if (Input.GetKeyDown(KeyCode.Space))
        {
            AudioManager.Asio.StopAll();
            StreamID = 0;
        }
    }
}
