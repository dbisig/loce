﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LowLatencyMultichannelAudio;

public class Play_Sound : MonoBehaviour
{

  //  public AN_SoundCircular;
    public bool EventSoundCircular;
    public bool EventSoundCircularDouble;
    public bool EventSoundInverseCircular;
    public bool EventSoundStereo;
    public bool EventSoundStarted;
    public bool EventSoundTabL1;
    public bool EventSoundTabL2;
    public bool EventSoundTabL3;
    public float WaitTime;
    public int NbrSound;
    public int NbrSound2;
    public int NbrChannel;
    public int NbrChannel2;
    public int NbrChannel3;

    public int i;
    public int i2;
    public int i3;
    /*  public bool T1;
      public bool T2;
      public bool T3;
      public bool T4;
      public bool T5;
      public bool T6;
      public bool T7;
      public bool T8;*/
    float frac(float s) { return s - Mathf.Floor(s); }

    // Start is called before the first frame update
    void Start()
    {
       EventSoundCircular = false;
       EventSoundCircularDouble = false;
       EventSoundInverseCircular = false;
       EventSoundStereo = false;

      // AudioManager.Asio.Play(1, new[] { 1,2 }, 1, 0, true);          //  AudioManager.Asio.Stop( 3, 0.6f);
        i = 0;
       i2 = 0;
       i3 = 0;
       NbrChannel = 0;
       NbrChannel2 = 0;
       NbrChannel3 = 9;
    }

    // Update is called once per frame
    void Update()
    {
        if (EventSoundTabL1 == true)
        {
            NbrChannel = 0;
            i = 0;
            StartCoroutine("TabL1");
            EventSoundTabL1 = false;
        }
        if (EventSoundTabL2 == true)
        {
            NbrChannel2 = 0;
            i2 = 0;
            StartCoroutine("TabL2");
            EventSoundTabL2 = false;
        }
        if (EventSoundTabL3 == true)
        {
            NbrChannel3 = 0;
            i3 = 0;
            StartCoroutine("TabL3");
            EventSoundTabL3 = false;
        }
       
        if (EventSoundCircular==true)
        {
            NbrChannel = 0;
            i = 0;
            StartCoroutine("Circular");
            EventSoundCircular = false;
        }
        if (EventSoundCircularDouble == true)
        {
            NbrChannel = 0;
            i = 0;
            StartCoroutine("CircularDouble");
            EventSoundCircularDouble = false;
        }
        if (EventSoundInverseCircular == true)
        {
            NbrChannel = 0;
            i = 0;
            StartCoroutine("InverseCircular");
            EventSoundInverseCircular = false;
        }
        if (EventSoundStereo==true)
        {
            AudioManager.Asio.Play(NbrSound, new[] { 3,11 }, 1, 0.2f, false);
            AudioManager.Asio.Play(NbrSound2, new[] { 6, 14 }, 1, 0.2f, false);
            EventSoundStereo = false;
        }        
    }

    /// ////////////////////////////////////////////////////Start 
    public IEnumerator Started()
    {
        AudioManager.Asio.Play(7, new[] { 1, 2 }, 0.5f, 0, false);
        yield return null;
    }

    /// ////////////////////////////////////////////////////Phase 01 PHOTO
        IEnumerator TabL1()
        {
        while (i < 6)
        {
            AudioManager.Asio.Play(NbrSound, new[] { NbrChannel }, 0.6f, 0.1f, false);
            NbrChannel++;
            i++;
            yield return new WaitForSeconds(0.27f);
        }
        if (i == 6)
        {
            EventSoundTabL1 = true;
        }
        }
        IEnumerator TabL2()
        {
            while (i2 < 6)
            {
                AudioManager.Asio.Play(NbrSound, new[] { NbrChannel2 }, 0.6f, 0.1f, false);
                NbrChannel2++;
                i2++;
                yield return new WaitForSeconds(0.55f);
            }
            if (i2 == 6)
            {
               EventSoundTabL2 = true;
            }
        }
        IEnumerator TabL3()
        {
            while (i3 < 6)
            {
                AudioManager.Asio.Play(NbrSound, new[] { NbrChannel3 }, 0.6f, 0.1f, false);
                NbrChannel3++;
                i3++;
                yield return new WaitForSeconds(1.1f);
            }
            if (i3 == 6)
            {
                EventSoundTabL3 = true;
            }
        }

        public IEnumerator Touch01()
        {
        AudioManager.Asio.Play(7, new[] { 5,13 }, 0.5f, 0.1f, false);
        yield return null;
        }

        public IEnumerator Touch02()
        {
        AudioManager.Asio.Play(8, new[] { 4, 12 }, 0.5f, 0.1f, false);
        yield return null;
        }

    IEnumerator Circular()
    {
        while (i<17)
        {
            Debug.Log("Circular");
            AudioManager.Asio.Play(NbrSound, new[] { NbrChannel }, 0.35f, 0.1f, false);
            NbrChannel++;
            i++;
            yield return new WaitForSeconds(WaitTime);
        }
    }

    IEnumerator CircularDouble()
    {
        while (i < 9)
        {
            AudioManager.Asio.Play(NbrSound, new[] { NbrChannel }, 1, 0, false);
            AudioManager.Asio.Play(NbrSound, new[] { 8+NbrChannel }, 1, 0, false);
            NbrChannel++;
            i++;
            yield return new WaitForSeconds(WaitTime);
        }
    }

    IEnumerator InverseCircular()
    {
        while (i < 9)
        {
            AudioManager.Asio.Play(NbrSound, new[] { NbrChannel }, 0.5f, 0.2f, false);
            AudioManager.Asio.Play(NbrSound, new[] { 15 - NbrChannel }, 0.5f, 0.2f, false);
            NbrChannel++;
            i++;
            yield return new WaitForSeconds(WaitTime);
        }
    }
}
