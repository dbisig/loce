﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUI_Display : MonoBehaviour
{
    public GettingStartedReceiving S_TouchInfo;

    public GameObject Traker;
    // Rule for Sound Var value
    public int limitX;
    public int limitY;
    public float TransitionValue;

    //Zone Screen var
    public bool ZoneA = false;
    public bool ZoneB = false;
    public bool ZoneC = false;
    public bool ZoneD = false;

    // Function Custom 
    float map(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }
    //public script GettingStartedReceiving;
    [SerializeField]
    public Text TextA1 = null;
    [SerializeField]
    public Text TextA2 = null;
    [SerializeField]
    public Text TextA3 = null;
    [SerializeField]
    public Text TextA4 = null;
    [SerializeField]
    public Text TextA5 = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }
/*
    void Update()
    {
        TextA1.text = S_TouchInfo.PointPosition[0].ToString();
        TextA2.text = S_TouchInfo.PointPosition[1].ToString();
        if (Input.GetKeyDown("f"))
        {
            TextA1.text = S_TouchInfo.PointPosition[0].ToString();
            TextA2.text = S_TouchInfo.PointPosition[1].ToString();
        }
    }*/

    void Update()
    {
        if (Traker.transform.position.y > limitY)
        {
            ZoneC = false;
            ZoneD = false;

            if (Traker.transform.position.x < limitX)
            {
                ZoneB = false;
                ZoneA = true;
            }
            else
            {
                ZoneA = false;
                ZoneB = true;
            }

        }
        else
        {
            ZoneA = false;
            ZoneB = false;
            if (Traker.transform.position.x < limitX)
            {
                ZoneD = false;
                ZoneC = true;
            }
            else
            {
                ZoneC = false;
                ZoneD = true;
            }
        }
        /////////////////////////////////////////////////////////   ChekerZone
        Renderer R = Traker.GetComponent<Renderer>();

        if (ZoneA == true)
        {
            R.material.color = Color.red;
        }
        else if (ZoneB == true)
        {
            R.material.color = Color.blue;
        }
        else if (ZoneC == true)
        {
            R.material.color = Color.green;
        }
        else if (ZoneD == true)
        {
            R.material.color = Color.white;
        }



    }
}
