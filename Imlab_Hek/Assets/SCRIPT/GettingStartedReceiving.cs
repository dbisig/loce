﻿
using UnityEngine;
using System.Collections;

//namespace OscSimpl.Examples
//{
    public class GettingStartedReceiving : MonoBehaviour
    {
        [SerializeField] OscIn _oscIn;
        public int NbrPort;
       // const string address1 = "/test1";
       // const string address2 = "/test2";
        const string address3 = "/tuio/2Dcur";
        string _incomingText;
        float _incomingFloat;
        float _incomingFloat2;
        float _incomingFloat3;
        float _incomingFloat4;
        int _incomingInt;
        int _incomingInt2;
        int _incomingInt3;
        int _incomingInt4;
        public bool Touchenable;
        public bool Touch01 = false;
        public Vector2 FTP01 = new Vector2(0, 0);
        public bool Touch02 = false;
        public Vector2 FTP02 = new Vector2(0, 0);
        public bool Touch03 = false;
        public Vector2 FTP03 = new Vector2(0, 0);
        public bool Touch04 = false;
        public Vector2 FTP04 = new Vector2(0, 0);
        public bool Touch05 = false;
        public Vector2 FTP05 = new Vector2(0, 0);
        public bool Touch06 = false;
        public Vector2 FTP06 = new Vector2(0, 0);
        public Vector2[] PointPosition;
        public Vector2[] VelocityInfo;
        public int T1 = 0;
        public bool T1ok ;
        public int T2 = 0;
    public bool T2ok;
    int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;

    void Start()
        {
        Touchenable = false;
        T1ok = false;
        T2ok = false;
        Touch01 = false;
        FTP01 = new Vector2(0, 0);
        Touch02 = false;
        FTP02 = new Vector2(0, 0);
        Touch03 = false;
        FTP03 = new Vector2(0, 0);
        Touch04 = false;
        FTP04 = new Vector2(0, 0);
        Touch05 = false;
        FTP05 = new Vector2(0, 0);
        Touch06 = false;
        FTP06 = new Vector2(0, 0);
        // if (!_oscIn) _oscIn = gameObject.AddComponent<OscIn>();            // Ensure that we have a OscIn component and start receiving on port 7000.
        _oscIn.Open(NbrPort);
        }

    void Update()
    {
        /*    if (T1ok == true)
            {
                T1ok = false;
            }else if (T2ok == true)
            {
                T2ok = false;
            }*/
        if (Touchenable == true)
        {
            if (Touch01 == true)
            {
                FTP01 = PointPosition[0];
                StartCoroutine("RecT01");
                Touch01 = false;
            }
            else if (Touch02 == true)
            {
                FTP02 = PointPosition[1];
                StartCoroutine("RecT02");
                Touch02 = false;
            }
            else if (Touch03 == true)
            {
                FTP03 = PointPosition[2];
                StartCoroutine("RecT03");
                Touch03 = false;
            }
            else if (Touch04 == true)
            {
                FTP04 = PointPosition[3];
                StartCoroutine("RecT04");
                Touch04 = false;
            }
            else if (Touch05 == true)
            {
                FTP05 = PointPosition[4];
                StartCoroutine("RecT05");
                Touch05 = false;
            }
            else if (Touch06 == true)
            {
                FTP06 = PointPosition[5];
                StartCoroutine("RecT06");
                Touch06 = false;
            }
        }
    }
    void OnMessageReceived(OscMessage incomingMessage)
        {
            // Try to get the string from the message at arg index 0.
            if (incomingMessage.TryGet(0, ref _incomingText))
            {
                // We have now received a string that will only be
                // recreated (generate garbage) if it changes.
                // However, this Debug.Log call will generate garbage. Lots of it ;)
             //   Debug.Log(_incomingText);
            }
        }
            void OnEnable()
        {
            // You can "map" messages to methods in two ways:
            // 1) For messages with a single argument, route the value using the type specific map methods.
           // _oscIn.MapFloat(address1, OnTest1);
            _oscIn.Map(address3, OnTest3);// 2) For messages with multiple arguments, route the message using the Map method.
        }

        void OnDisable()
        {
            // If you want to stop receiving messages you have to "unmap".
            _oscIn.Unmap(OnTest2);
        }

        void OnTest2(OscMessage message)
        {
            // Get arguments from index 0, 1 and 2 safely.
            int frameCount;
            float time;
            float random;
            if (
                message.TryGet(0, out frameCount) &&
                message.TryGet(1, out time) &&
                message.TryGet(2, out random)
            )
            {
                Debug.Log("Received test2\n" + frameCount + " " + time + " " + random + "\n");
            }
            // If you don't know what type of arguments to expect, then you 
            // can check the type of each argument and get the ones you need.
            for (int i = 0; i < message.Count(); i++)
            {
                OscArgType argType;
                if (!message.TryGetArgType(i, out argType)) continue;

                switch (argType)
                {
                    case OscArgType.Float:
                        float floatValue;
                        message.TryGet(i, out floatValue);
                        // Do something with floatValue here...
                        break;
                    case OscArgType.Int:
                        int intValue;
                        message.TryGet(i, out intValue);
                        // Do something with intValue here...
                        break;
                }
            }
           // Always recycle incoming messages when used.
            OscPool.Recycle(message);
        }           
            void OnTest3(OscMessage incomingMessage)
            {
            if (incomingMessage.TryGet(0, ref _incomingText))
            {
                Debug.Log(_incomingText);
            }
            if (incomingMessage.TryGet(0, ref _incomingText))
            {
            if (_incomingText == "born")
            {
                if (incomingMessage.TryGet(1, out _incomingInt))
                {
                    if (_incomingInt == 0)
                    {
                        PointPosition[0] = new Vector2(0, 0);
                        VelocityInfo[0] = new Vector2(0, 0);
                        Touch01 = true;
                    }else if (_incomingInt == 1)
                    {
                        PointPosition[1] = new Vector2(0, 0);
                        VelocityInfo[1] = new Vector2(0, 0);
                        Touch02 = true;
                    } else if (_incomingInt == 2)
                    {
                        PointPosition[2] = new Vector2(0, 0);
                        VelocityInfo[2] = new Vector2(0, 0);
                        Touch03 = true;
                    } else if (_incomingInt == 3)
                    {
                        PointPosition[3] = new Vector2(0, 0);
                        VelocityInfo[3] = new Vector2(0, 0);
                        Touch04 = true;
                    } else if (_incomingInt == 4)
                    {
                        PointPosition[4] = new Vector2(0, 0);
                        VelocityInfo[4] = new Vector2(0, 0);
                        Touch05 = true;
                    } else if (_incomingInt == 5)
                    {
                        PointPosition[5] = new Vector2(0, 0);
                        VelocityInfo[5] = new Vector2(0, 0);
                        Touch06 = true;
                    }
                }
            }
                    if (_incomingText == "set")
                {
                    if (incomingMessage.TryGet(1, out _incomingInt) && incomingMessage.TryGet(2, out _incomingFloat) && incomingMessage.TryGet(3, out _incomingFloat2) && incomingMessage.TryGet(3, out _incomingFloat3) && incomingMessage.TryGet(3, out _incomingFloat4))
                    {
                        if (_incomingInt == 0)
                        {
                            PointPosition[0] = new Vector2(_incomingFloat, _incomingFloat2);
                            VelocityInfo[0] = new Vector2(_incomingFloat3, _incomingFloat4);
                            //Touch01 = true;
                        }
                        else if (_incomingInt == 1)
                        {
                            PointPosition[1] = new Vector2(_incomingFloat, _incomingFloat2);
                            VelocityInfo[1] = new Vector2(_incomingFloat3, _incomingFloat4);
                            //Touch02 = true;
                        }
                        else if (_incomingInt == 2)
                        {
                            PointPosition[2] = new Vector2(_incomingFloat, _incomingFloat2);
                            VelocityInfo[2] = new Vector2(_incomingFloat3, _incomingFloat4);
                            //Touch03 = true;
                        }
                        else if (_incomingInt == 3)
                        {
                            PointPosition[3] = new Vector2(_incomingFloat, _incomingFloat2);
                            VelocityInfo[3] = new Vector2(_incomingFloat3, _incomingFloat4);
                           // Touch04 = true;
                        }
                        else if (_incomingInt == 4)
                        {
                            PointPosition[4] = new Vector2(_incomingFloat, _incomingFloat2);
                            VelocityInfo[4] = new Vector2(_incomingFloat3, _incomingFloat4);
                           // Touch05 = true;
                        }
                        else if (_incomingInt == 5)
                        {
                            PointPosition[5] = new Vector2(_incomingFloat, _incomingFloat2);
                            VelocityInfo[5] = new Vector2(_incomingFloat3, _incomingFloat4);
                           // Touch06 = true;
                        }
                    }
                }

                if (_incomingText == "died")
                {
                    if (incomingMessage.TryGet(1, out _incomingInt))
                    {
                        if (_incomingInt == 0)
                        {
                            PointPosition[0] = new Vector2(0, 0);
                            VelocityInfo[0] = new Vector2(0, 0);
                            Touch01 = false;
                        }
                        else if (_incomingInt == 1)
                        {
                            PointPosition[1] = new Vector2(0, 0);
                            VelocityInfo[1] = new Vector2(0, 0);
                            Touch02 = false;
                        }
                        else if (_incomingInt == 2)
                        {
                            PointPosition[2] = new Vector2(0, 0);
                            VelocityInfo[2] = new Vector2(0, 0);
                            Touch03 = false;
                        }
                        else if (_incomingInt == 3)
                        {
                            PointPosition[3] = new Vector2(0, 0);
                            VelocityInfo[3] = new Vector2(0, 0);
                            Touch04 = false;
                        }
                        else if (_incomingInt == 4)
                        {
                            PointPosition[4] = new Vector2(0, 0);
                            VelocityInfo[4] = new Vector2(0, 0);
                            Touch05 = false;
                        }
                        else if (_incomingInt == 5)
                        {
                            PointPosition[5] = new Vector2(0, 0);
                            VelocityInfo[5] = new Vector2(0, 0);
                            Touch06 = false;
                        }
                    }
                }

                if (_incomingText == "alive")
                {
                    if (incomingMessage.TryGet(1, out _incomingInt))
                    {

                    }else
                    {
                        PointPosition[0] = new Vector2(0, 0);
                        VelocityInfo[0] = new Vector2(0, 0);
                       // Debug.Log("DebugTouche1");
                        Touch01 = false;
                    }
                    if (incomingMessage.TryGet(2, out _incomingInt))
                    {

                    }else
                    {
                        PointPosition[1] = new Vector2(0, 0);
                        VelocityInfo[1] = new Vector2(0, 0);
                       // Debug.Log("DebugTouche2");
                        Touch02 = false;
                    }
                    if (incomingMessage.TryGet(3, out _incomingInt))
                    {

                    }else
                    {
                        PointPosition[2] = new Vector2(0, 0);
                        VelocityInfo[2] = new Vector2(0, 0);
                      //  Debug.Log("DebugTouche3");
                        Touch03 = false;
                    }
                    if (incomingMessage.TryGet(4, out _incomingInt))
                    {
                    }else
                    {
                        PointPosition[3] = new Vector2(0, 0);
                        VelocityInfo[3] = new Vector2(0, 0);
                      //  Debug.Log("DebugTouche4");
                        Touch04 = false;
                    }
                    if (incomingMessage.TryGet(5, out _incomingInt))
                    {
                    }else
                    {
                        PointPosition[4] = new Vector2(0, 0);
                        VelocityInfo[4] = new Vector2(0, 0);
                        //Debug.Log("DebugTouche5");
                        Touch05 = false;
                    }
                    if (incomingMessage.TryGet(4, out _incomingInt))
                    {
                    }else
                    {
                        PointPosition[5] = new Vector2(0, 0);
                        VelocityInfo[5] = new Vector2(0, 0);
                      //  Debug.Log("DebugTouche5");
                        Touch05 = false;
                    }
                }
            }

            OscPool.Recycle(incomingMessage);
    }

    IEnumerator RecT01()
    {      
        while (i < 1)
        {
            if (PointPosition[0].x < 1)
            {
                T1ok = true;
                T1++;
            }
            else if (PointPosition[0].x > 3)
            {
                T2ok = true;
                T2++;
            }
            yield return new WaitForSeconds(0.3f);
            T2ok = false;
            T1ok = false;
            i++;
            FTP01 = new Vector2(0, 0);
        }
        i = 0;
    }
    IEnumerator RecT02()
    {
        while (i2 < 1)
        {
            if (PointPosition[1].x < 1)
            {
                T1ok = true;
                T1++;
            }
            else if (PointPosition[1].x > 3)
            {
                T2ok = true;
                T2++;
            }
            yield return new WaitForSeconds(0.3f);
            T2ok = false;
            T1ok = false;
            i2++;
            FTP02 = new Vector2(0, 0);
        }
        i2 = 0;
    }
    IEnumerator RecT03()
    {
        while (i3 < 1)
        {
            if (PointPosition[2].x < 1)
            {
                T1ok = true;
                T1++;
            }
            else if (PointPosition[2].x > 3)
            {
                T2ok = true;
                T2++;
            }
            yield return new WaitForSeconds(0.3f);
            T2ok = false;
            T1ok = false;
            i3++;
            FTP03 = new Vector2(0, 0);
        }
        i3 = 0;
    }
    IEnumerator RecT04()
    {
        while (i4 < 1)
        {
            if (PointPosition[3].x < 1)
            {
                T1ok = true;
                T1++;
            }
            else if (PointPosition[3].x > 3)
            {
                T2ok = true;
                T2++;
            } 
            yield return new WaitForSeconds(0.3f);
            T2ok = false;
            T1ok = false;
            i4++;
            FTP04 = new Vector2(0, 0);
        }
        i4 = 0;
    }
    IEnumerator RecT05()
    {
        while (i5 < 1)
        {
            if (PointPosition[4].x < 1)
            {
                T1ok = true;
                T1++;
            }
            else if (PointPosition[4].x > 3)
            {
                T2ok = true;
                T2++;
            }
            yield return new WaitForSeconds(0.3f);
            T2ok = false;
            T1ok = false;
            i5++;
            FTP05 = new Vector2(0, 0);
        }
        i5 = 0;
    }
    IEnumerator RecT06()
    {
        while (i6 < 1)
        {
           if (PointPosition[5].x < 1)
            {
                T1ok = true;
                T1++;
            }
            else if (PointPosition[5].x > 3)
            {
                T2ok = true;
                T2++;
            }
            yield return new WaitForSeconds(0.3f);
            T2ok = false;
            T1ok = false;
            i6++;
            FTP05 = new Vector2(0, 0);
        }
        i6 = 0;
    }

    //  NbrOPointDetected =1;
    // NbrOPoint = new Vector2[NbrOPointDetected];
    //  incomingMessage.TryGet(2, out _incomingFloat);
    //   incomingMessage.TryGet(3, out _incomingFloat2);
    /*
    if (incomingMessage.TryGet(1, out _incomingInt))
    { 

        Debug.Log(_incomingInt);
    }
    if (incomingMessage.TryGet(2, out _incomingFloat))
    {
        // We have now received a string that will only be
        // However, this Debug.Log call will generate garbage. Lots of it ;)
        Debug.Log(_incomingFloat);
    }
    if (incomingMessage.TryGet(3, out _incomingFloat))
    {
        // We have now received a string that will only be
        // However, this Debug.Log call will generate garbage. Lots of it ;)
        Debug.Log(_incomingFloat);
    }
    */
    // We have now received a string that will only be
    // recreated (generate garbage) if it changes.
    // However, this Debug.Log call will generate garbage. Lots of it ;)

    /*      // Get arguments from index 0, 1 and 2 safely.
          float S;
          int frameCount;
          float t;
          float r;
          if (
              message.TryGet(0, out S) &&
              message.TryGet(1, out t) &&
              message.TryGet(2, out r)
          )
          {
              Debug.Log("Received test3\n" + S + " " + t + " " + r + "\n");
          }*/

}
//}