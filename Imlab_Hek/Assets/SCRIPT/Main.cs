﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LowLatencyMultichannelAudio;


public class Main : MonoBehaviour
{
   // private const int V = 10;
    public GettingStartedReceiving S_TouchPos;
    public Play_Sound S_SoundControl;
    public detecvignette S_Detec;
    //public ComputeShader detecS;
    public string State;
    public float Counter;
    public float TransitionTime;
    public int Step;
    public bool Started;
    private bool TouchSound;
    private float Count;
    public float Duration;
    public float TimePhase01;
    public float TimePhase02;
    public float TimePhase03;
    public float TimeBeforeEnd;
    public float Value;
    private int i;
    private int i2;

    void Start()
    {
        i = 8;
        i2 = 8;
        AudioManager.Asio.Play(1, new[] { 0, 1 }, 1, 0, true);
        Count = 0f;
        Value = 0f;
        Started = false;
        State = "Nean";
        Step = 0;
    }

    void Update()
    {
        ////////////////////////////////////////////////////Transition 1 & 2 Sound

        if (Mathf.Floor(Value * 10) / 10 == 0.1 && i == 0)
        {
            AudioManager.Asio.Play(11, new[] { 0, 1, 15, 7 }, 0.8f, 0f, false);
            i++;
        }
        else if (Mathf.Floor(Value * 10) / 10 == 0.8 & i == 1)
        {
            AudioManager.Asio.Play(7, new[] { 9, 10 }, 0.6f, 0.1f, false);
            i++;
        }
        else if (Mathf.Floor(Value * 10) / 10 == 0.9 & i == 2)
        {
            AudioManager.Asio.Play(8, new[] { 9, 10 }, 1f, 0, false);
            i++;
        }

        ////////////////////////////////////////////////////Transition 4 Sound
        float x = Mathf.Floor(Value * 8) / 8;
        if (x == 0.125 && i2 == 0)
        {
        AudioManager.Asio.Play(8, new[] { 1,9 }, 0.5f, 0.1f, false);
        i2++;
        }
        else if (x == 0.25 && i2 == 1)
        {
        AudioManager.Asio.Play(7, new[] { 0, 1, 15, 7 }, 0.5f, 0f, false);
        i2++;
        }
        else if (x == 0.375 && i2 == 2)
        {
        AudioManager.Asio.Play(8, new[] { 3, 11 }, 0.5f, 0, false);
        i2++;
        }
        else if (x == 0.5 && i2 == 3)
        {
        AudioManager.Asio.Play(7, new[] { 0, 1, 15, 7 }, 0.5f, 0.1f, false);
        i2++;
        }
                else if (x == 0.625 && i2 == 4)
                 {
            AudioManager.Asio.Play(8, new[] { 9, 13 }, 0.5f, 0.1f, false);
            i2++;
                }
                else if (x == 0.75 && i2 == 5)
                {
            AudioManager.Asio.Play(7, new[] { 0, 1, 15, 7 }, 0.5f, 0.1f, false);
            i2++;
                }
                else if (x == 0.875 && i2 == 6)
                {
            AudioManager.Asio.Play(8, new[] { 4, 15 }, 0.5f, 0.1f, false);
            i2++;
                }

        if (TouchSound == true)
        {
            if (S_TouchPos.T1ok == true)
            {
                S_SoundControl.StartCoroutine("Touch01");
                S_TouchPos.T1ok = false;
            }
            if (S_TouchPos.T2ok == true)
            {
                S_SoundControl.StartCoroutine("Touch02");
                S_TouchPos.T2ok = false;
            }
        }
        if (S_Detec.detection != 0 && State == "Nean")
        {
           Started = true;
        }
                ////////////////////////COUNTER ON
                if (State == "Phase01")
                {
                    Counter += Time.deltaTime;
                }
                else if (State == "Phase02")
                {
                    Counter += Time.deltaTime;
                }
                else if (State == "Phase03")
                {
                    Counter += Time.deltaTime;
                }else if ( State == "TransitionFin")
                {
                    Counter += Time.deltaTime;
                }

                ////////////////////////Start
                if (Started == true)
                {

                    State = "Start";
                    S_Detec.Searching = false;
                    S_SoundControl.StartCoroutine("Started");
                    StartCoroutine("SliderAnim0");
                    Step++;
                    Started = false;
                }
                ////////////////////////Phase 01 PHOTO
                if (Step == 1 && State == "Start")
                {
           // S_SoundControl.NbrChannel = 0;
           // S_SoundControl.NbrChannel2 = 0;
          //  S_SoundControl.NbrChannel3 = 0;
            S_SoundControl.NbrSound = 8;
            S_SoundControl.EventSoundTabL1 = true;
            S_SoundControl.EventSoundTabL2 = true;
            S_SoundControl.EventSoundTabL3 = true;
            //S_SoundControl.StartCoroutine("TabL1");
            //S_SoundControl.StartCoroutine("TabL2");
            //S_SoundControl.StartCoroutine("TabL3");
            State = "Phase01";
            Step++;
                }
                ////////////////////////Transition
                if (Counter > TimePhase01 && State == "Phase01")
                {
                    //  AudioManager.Asio.Stop(8, 0.1f);
                    i = 0;
                    Value = 0f;
                    Count = 0f;
                    // S_SoundControl.NbrSound = 6;
                    StartCoroutine("SliderAnim1");
                    S_SoundControl.StartCoroutine("Started");
            S_SoundControl.EventSoundTabL1 = false;
            S_SoundControl.EventSoundTabL2 = false;
            S_SoundControl.EventSoundTabL3 = false;
            S_SoundControl.StopCoroutine("TabL1");
                    S_SoundControl.StopCoroutine("TabL2");
                    S_SoundControl.StopCoroutine("TabL3");
                    State = "Transition";

                    // S_SoundControl.EventSoundInverseCircular = true;
                    StartCoroutine("NextStep");
                }
                ////////////////////////Phase 02 HAND
                if (Step == 3)
                {
            S_SoundControl.NbrSound = 6;
                    S_SoundControl.EventSoundCircular = true;
                    TouchSound = true;
                    S_TouchPos.Touchenable = true;
                    //S_SoundControl.StartCoroutine("Started");
                    State = "Phase02";
                    Step++;
                }

                ////////////////////////Transition
                if (Counter > TimePhase02 && State == "Phase02")
                {
                    i = 0;
                    Value = 0f;
                    Count = 0f;
                    // S_SoundControl.NbrSound = 5 ;
                    S_TouchPos.Touchenable = false;
                    TouchSound = false;
                    S_SoundControl.StartCoroutine("Started");
                    State = "Transition";
                    StartCoroutine("SliderAnim2");
                    //S_SoundControl.NbrSound = 6;
                    //  S_SoundControl.StartCoroutine("InverseCircular");
                    StartCoroutine("NextStep");
                }
                ////////////////////////Phase 03 Fantome
                if (Step == 5)
                {
                   // S_SoundControl.StartCoroutine("Started");
                    S_SoundControl.NbrSound = 5;
                    S_SoundControl.EventSoundCircular = true;
                    // AudioManager.Asio.Play(2, new[] { 5, 6 }, 0.4f, 0.1f, false);
                    // AudioManager.Asio.Play(4, new[] {  8,9,10,11,12,13,14,15 }, 0.15f, 0.1f, false);
                    State = "Phase03";
                    Step++;
                }
                ////////////////////////Transition
                if (Counter > TimePhase03 && State == "Phase03")
                {
                    i2 = 0;
                    Value = 0f;
                    Count = 0f;
                    S_SoundControl.StartCoroutine("Started");
                    State = "TransitionFin";
                    StartCoroutine("SliderAnim3");
                    //S_SoundControl.NbrSound = 6;
                    //  S_SoundControl.StartCoroutine("InverseCircular");
                    StartCoroutine("NextStep");
                }

           if (Counter > TimeBeforeEnd && State == "TransitionFin")
           {
           Debug.Log("FINISH");
            S_TouchPos.T1 = 0;
            S_TouchPos.T2 = 0;
            Step = 0;
            i = 8;
            i2 = 8;
            S_Detec.animation0 = 0;
            S_Detec.animation1 = 0;
            S_Detec.animation2 = 0;
            S_Detec.animation3 = 0;
            Counter = 0;
            Value = 0;
            State = "Nean";
            S_Detec.Searching = true;
           }
                /* ////////////////////////Phase 04 Vignette
                  if (Step == 7)
                  {
                      S_SoundControl.StartCoroutine("Started");
                      S_SoundControl.NbrSound = 5;
                      S_SoundControl.EventSoundCircular = true;
                      //  AudioManager.Asio.Play(2, new[] { 5, 6 }, 0.4f, 0.1f, false);
                      // AudioManager.Asio.Play(4, new[] {  8,9,10,11,12,13,14,15 }, 0.15f, 0.1f, false);
                      State = "Phase04";
                      Step++;
                  }
                 ////////////////////////Transition
                 if (Counter > TimePhase03 && State == "Phase04")
                 {
                     i = 0;
                     Value = 0f;
                     Count = 0f;
                     S_SoundControl.StartCoroutine("Started");
                     State = "TransitionFin";
                     StartCoroutine("SliderAnim4");
                     //S_SoundControl.NbrSound = 6;
                     //  S_SoundControl.StartCoroutine("InverseCircular");
                     StartCoroutine("NextStep");
                 }*/

                ////////////////////////Son ponctuel Ambiance
         /*       if (Mathf.Floor(Counter) == +30 && State == "Phase01")
                {
                    Debug.Log("TestSoundponctuelOK - Phase 1");
                    S_SoundControl.NbrSound = 4;
                    S_SoundControl.EventSoundCircular = true;
                    Counter++;
                    //  AudioManager.Asio.Play(10, new[] { 7, 0, 4, 3 }, 0.4f, 0.2f, false);
                }*/
                if (Mathf.Floor(Counter) == TimePhase01 + 35 && State == "Phase02")
                {
                    Debug.Log("TestSoundponctuelOK - Phase 2");
                    S_SoundControl.NbrSound = 6;
                    S_SoundControl.EventSoundCircular = true;
                    Counter++;
                    //  AudioManager.Asio.Play(10, new[] { 7, 0, 4, 3 }, 0.4f, 0.2f, false);
                }
                if (Mathf.Floor(Counter) == TimePhase02 + 45 && State == "Phase03")
                {
                    Debug.Log("TestSoundponctuelOK - Phase3");
                    AudioManager.Asio.Play(5, new[] { 7, 0, 4, 3 }, 0.4f, 0.1f, false);
                }

                if (Mathf.Floor(Counter) == TimePhase03 + 30 && State == "Phase04")
                {
                    Debug.Log("TestSoundponctuelOK - Phase4");
                    AudioManager.Asio.Play(6, new[] { 7, 0, 4, 3 }, 0.4f, 0.1f, false);
                }
            }
    

    ////////////////////////NextStep
    IEnumerator NextStep()
    {
        yield return new WaitForSeconds(TransitionTime);
        Step++;
    }

    ////////////////////////Transition 0
    IEnumerator SliderAnim0()
    {
        while (Value < 1)
        {
            Value = Count;
            S_Detec.animation0 = Count;
            Count = Count + 0.1f;
            yield return new WaitForSeconds(0.1f);
        }
    }

    ////////////////////////Transition 1
    IEnumerator SliderAnim1()
    {
        while (Value< 1)
        {
            Value = Count;
            S_Detec.animation1 = Count;
            Count = Count + 0.1f;
            yield return new WaitForSeconds(0.15f);
        }
    }

    ////////////////////////Transition 1
    IEnumerator SliderAnim2()
    {
        while (Value < 1)
        {
            Value = Count;
            S_Detec.animation2 = Count;
            Count = Count + 0.1f;
            yield return new WaitForSeconds(0.15f);
        }
    }

    ////////////////////////Transition 1
    IEnumerator SliderAnim3()
    {
        while (Value < 1)
        {
            Value = Count;
            S_Detec.animation3 = Count;
            Count = Count + 0.01f;
            yield return new WaitForSeconds(0.16f);
        }
    }


    /*      ////////////////////////TEST

      if (Input.GetKeyDown("s"))
      {
          S_SoundControl.NbrSound = 4;
          S_SoundControl.EventSoundStereo = true;
      }
      if (Input.GetKeyDown("i"))
      {
          S_SoundControl.NbrSound = 2;
          S_SoundControl.EventSoundInverseCircular = true;
      }
      if (Input.GetKeyDown("d"))
      {
          S_SoundControl.NbrSound = 7;
          S_SoundControl.EventSoundCircularDouble = true;
      }

      if (Input.GetKeyDown("r"))
      {
          Application.LoadLevel(Application.loadedLevel);
      }

      if (Input.GetKeyDown("space"))
      {

      }*/

}
