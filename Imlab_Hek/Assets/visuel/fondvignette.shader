﻿Shader "Unlit/fondvignette"
{
    Properties
    {
       // _MainTex ("Texture", 2D) = "white" {}
		_texf("_texf", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
             //   UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

           // sampler2D _MainTex;
           // float4 _MainTex_ST;
			sampler2D _texf;
			float4 _texf_ST;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _texf);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_texf, i.uv);
			float fac = 1920. / 1080.;
			float s1 = 0.0005;
			float s2 = 0.0003;
			float li = smoothstep(s1,s2,distance(0.5*fac, frac(i.uv.x*4.)*fac));
			float b1 = smoothstep(s1,s2, distance(0.25, i.uv.y));
			float b2 = smoothstep(s1, s2, distance(0.5, i.uv.y));
			float b3 = smoothstep(s1, s2, distance(0.75, i.uv.y));
			//float d1 = distance(0.5*fac, frac(i.uv.x*20. + 0.5)*fac);
			//float d2 = distance(0.5, frac(i.uv.y*5. + 0.5));
			//float cr1 =max(step(d1,0.0025)*step(d2,0.05), step(d2, 0.0025)*step(d1, 0.05))*0.2;
			float ca = max(0.,max(li,max( max(b1,b2),b3)));
			float f = max(col.x+ ca,col.y);
                return float4 (f,f,f,1.);
            }
            ENDCG
        }
    }
}
