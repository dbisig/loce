﻿using UnityEngine;

public class detecvignette : MonoBehaviour
{
    public ComputeShader compute_shader;
    public GettingStartedReceiving S_PosTouch;
    RenderTexture A;
    RenderTexture B;
    RenderTexture C;
    RenderTexture D;
    public Material material;
    public Texture tex;
    public Texture typo;
    public Texture typo2;
    Texture2D texture;
    int handle_main;
    public GameObject sp;
    public int resx = 7680;
    public int resy = 1080;
    public float _v1 = 0.0f;
    public float _v2 = 0.0f;
    public float _v3 = 0.0f;
    public float _v4 = 0.0f;
    public float _v5 = 0.0f;
    public float _v6 = 0.0f;
    public bool Searching = false;
    public float detection = 0.0f;
    public float animation0 = 0.0f;
    public float animation1 = 0.0f;
    public float animation2 = 0.0f;
    public float animation3 = 0.0f;
    public float animation4 = 0.0f;
    // private float PosTouch1;

    void Start()
    {
        
        Searching = true;
        A = new RenderTexture(resx, resy, 0);
        A.enableRandomWrite = true;
        A.Create();
        B = new RenderTexture(resx, resy, 0);
        B.enableRandomWrite = true;
        B.Create();
        C = new RenderTexture(resx, resy, 0);
        C.enableRandomWrite = true;
        C.filterMode = FilterMode.Point;
        C.Create();
        D = new RenderTexture(1, 1, 0);
        D.enableRandomWrite = true;
        D.filterMode = FilterMode.Point;
        D.Create();
        handle_main = compute_shader.FindKernel("CSMain");
        compute_shader.SetFloat("_resx", resx);
        compute_shader.SetFloat("_resy", resy);
        texture = new Texture2D(1, 1, TextureFormat.RGB24, false);
        compute_shader.SetTexture(handle_main, "typo", typo);
        compute_shader.SetTexture(handle_main, "typo2", typo2);
    }

    void Update()
    {
       
        compute_shader.SetTexture(handle_main, "reader", A);
        compute_shader.SetTexture(handle_main, "reader2", tex);
        
        compute_shader.SetFloat("_time", Time.time);
        compute_shader.SetFloat("_v1", _v1);
        compute_shader.SetFloat("_v2", _v2);
        compute_shader.SetFloat("_v3", _v3);
        compute_shader.SetFloat("_v4", _v4);
        compute_shader.SetFloat("_v5", _v5);
        compute_shader.SetFloat("_v6", _v6);
        compute_shader.SetFloat("_p1x", S_PosTouch.FTP01.x);
        compute_shader.SetFloat("_p1y", S_PosTouch.FTP01.y);
        compute_shader.SetFloat("_p2x", S_PosTouch.FTP02.x);
        compute_shader.SetFloat("_p2y", S_PosTouch.FTP02.y);
        compute_shader.SetFloat("_p3x", S_PosTouch.FTP03.x);
        compute_shader.SetFloat("_p3y", S_PosTouch.FTP03.y);
        compute_shader.SetFloat("_p4x", S_PosTouch.FTP04.x);
        compute_shader.SetFloat("_p4y", S_PosTouch.FTP04.y);
        compute_shader.SetFloat("_T1", S_PosTouch.T1);
        compute_shader.SetFloat("_T2", S_PosTouch.T2);
        compute_shader.SetFloat("_yolo", animation0);
        compute_shader.SetFloat("_animation1", animation1);
        compute_shader.SetFloat("_animation2", animation2);
        compute_shader.SetFloat("_animation3", animation3);
        compute_shader.SetFloat("_animation4", animation4);
        /*compute_shader.SetFloat("_p1x", S_PosTouch.PointPosition[0].x);
        compute_shader.SetFloat("_p1y", S_PosTouch.PointPosition[0].y);
        compute_shader.SetFloat("_p2x", S_PosTouch.PointPosition[1].x);
        compute_shader.SetFloat("_p2y", S_PosTouch.PointPosition[1].y);
        compute_shader.SetFloat("_p3x", S_PosTouch.PointPosition[2].x);
        compute_shader.SetFloat("_p3y", S_PosTouch.PointPosition[2].y);
        compute_shader.SetFloat("_p4x", S_PosTouch.PointPosition[3].x);
        compute_shader.SetFloat("_p4y", S_PosTouch.PointPosition[3].y);*/
        compute_shader.SetFloat("_delta", Time.deltaTime);
        compute_shader.SetTexture(handle_main, "writer", B);
        compute_shader.SetTexture(handle_main, "writer2", C);
        if (Searching == true)
        {
            compute_shader.SetTexture(handle_main, "writer3", D);
        }
        compute_shader.Dispatch(handle_main, A.width / 8, A.height / 8, 1);
        compute_shader.SetTexture(handle_main, "reader", B);
        compute_shader.SetTexture(handle_main, "writer", A);
        
        compute_shader.Dispatch(handle_main, B.width / 8, B.height / 8, 1);
        if (Searching==true)
        {
            Rect rectReadPicture = new Rect(0, 0, 1, 1);
            RenderTexture.active = D;
            texture.ReadPixels(rectReadPicture, 0, 0);
            texture.Apply();
            Vector4 det1 = texture.GetPixel(0, 0);
            detection = det1.x;
            
            
        }
        //material.SetTexture("_texf", C);
        sp.GetComponent<Spout.SpoutSender>().texture = C;
    }
}