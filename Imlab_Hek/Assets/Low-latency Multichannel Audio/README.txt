Low-latency Multichannel Audio for Unity / v1.0
===
Created by DataTunnel  

This assets is based on ASIO to provide low-latency multichannel audio playback. The latency can be less than 10 ms (If the operating environment is good, 1 ms or less can be realized).  

Please watch the official tutorial video - https://youtu.be/QKAp0vMdvu0  
It also available from official blog - https://gitpress.io/@data-tunnel/low-latency-multichannel-audio  

Contacts
---
E-mail: info@datatunnel.jp  
Blog: https://gitpress.io/@data-tunnel  
Github: https://github.com/Data-Tunnel  
