﻿using UnityEngine;
using LowLatencyMultichannelAudio;

public class KeyDownPlayer : MonoBehaviour
{
    private int StreamID;

    void Start()
    {
        Debug.Log("- Press Alpha1 - Alpha7 to play one shot.");
        Debug.Log("- Press Alpha8 to play looping, and Alpha9 to stop.");
        Debug.Log("- Press Space to stop all.");
    }

    void Update()
    {
        /* A. play one shot */
        if (Input.GetKeyDown(KeyCode.Alpha1))
            AudioManager.Asio.Play(3, new[] {  1 });

        if (Input.GetKeyDown(KeyCode.Alpha2))
            AudioManager.Asio.Play(3, new[] {  7 });

        if (Input.GetKeyDown(KeyCode.Alpha3))
            AudioManager.Asio.Play(3, new[] {  3 });

        if (Input.GetKeyDown(KeyCode.Alpha4))
            AudioManager.Asio.Play(4, new[] { 0, 4 });

        if (Input.GetKeyDown(KeyCode.Alpha5))
            AudioManager.Asio.Play(5, new[] { 0, 1 });

        if (Input.GetKeyDown(KeyCode.Alpha6))
            AudioManager.Asio.Play(6, new[] { 0, 1 });

        if (Input.GetKeyDown(KeyCode.Alpha7))
            AudioManager.Asio.Play(7, new[] { 0, 1 });

        /* B. play looping */
        if (Input.GetKeyDown(KeyCode.Alpha8) && StreamID <= 0)
            StreamID = AudioManager.Asio.Play(1, new[] { 0, 1 }, fadeSec: 1.0f, looping: true);

        /* C. stop B. */
        if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            AudioManager.Asio.Stop(StreamID, fadeSec: 1.0f);
            StreamID = 0;
        }

        /* D. stop all */
        if (Input.GetKeyDown(KeyCode.Space))
        {
            AudioManager.Asio.StopAll();
            StreamID = 0;
        }
    }
}
