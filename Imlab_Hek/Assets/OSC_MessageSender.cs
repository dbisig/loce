﻿

using UnityEngine;

namespace OscSimpl.Examples
{
	public class OSC_MessageSender : MonoBehaviour
	{
		public OscOut _oscOut;

		OscMessage _message; // Cached message.
        public int NbrPort_Info;
        public int Nbr_Info;
        const string address = "/trackerMaster/requestTuiostream";

		void Start()
		{

			_message = new OscMessage( address );

			_oscOut.Send( _message );
		}


		void Update()
		{

			_message.Set( 0, NbrPort_Info );
			_message.Set( 1, Nbr_Info );
			//_message.Set( 2, Random.value );
			_oscOut.Send( _message );
		}
	}
}